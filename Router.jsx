FlowRouter.route('/', {
  action() {
    ReactLayout.render(MainPage);
  }
});

FlowRouter.route('/jobs', {
  action(params) {
    ReactLayout.render(JobsPage);
  }
});

FlowRouter.route('/about', {
  action(params) {
    ReactLayout.render(AboutPage);
  }
});

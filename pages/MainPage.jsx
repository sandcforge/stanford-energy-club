const {
  AppCanvas,
} = MUI;


injectTapEventPlugin();

MainPage = React.createClass({

  render() {

  return (
      <div>
      <Header title="Stanford Energy Club"/>
      <AppCanvas>
        <br/>
        <NewsItem />
        <br/>
        <NewsItem />
        <br/>
        <Footer />
      </AppCanvas>
    </div>

      );
    }
  });

const {
  AppCanvas,
  Avatar,
  AppBar,
  Styles,
  RaisedButton,
  FlatButton,
  IconButton,
  Menu,
  MenuItem,
  MobileTearSheet,
  ListItem,
  List,
  Divider,
  TextField
} = MUI;

const {
  ImageDehaze,
  NavigationClose,
  ActionAccountCircle,
  ActionInfo,
  CommunicationChatBubble
} = SvgIcons;

const { ThemeManager, LightRawTheme, Colors } = Styles;

injectTapEventPlugin();

JobsPage = React.createClass({

  render() {
    const leftIconButton = (
      <IconButton
        onTouchTap={ () => this.refs.refLeftMenu.handleToggle() }
      >
        <ImageDehaze color={Colors.white}/>
      </IconButton>
    )

    const rightIconButton = (
      <IconButton
        onTouchTap={ () => this.refs.refLoginDialog._handleOpen() }
      >
        <ActionAccountCircle color={Colors.white}/>
      </IconButton>
    )


  return (
      <div>
      <Header title="Jobs"/>

      <AppCanvas>

            <List>
              <TextField
                fullWidth = {true}
                hintText="Search Jobs"
              />
              <ListItem
                primaryText="Research Associate"
                secondaryText="Aerial Intelligence"
                leftAvatar={<Avatar src="https://cdn4.iconfinder.com/data/icons/new-google-logo-2015/400/new-google-favicon-128.png" />}
                rightIcon={<CommunicationChatBubble />} />
              <Divider/>
              <ListItem
                primaryText="Project Manager"
                secondaryText="SEE Change Institute"
                leftAvatar={<Avatar src="https://www.facebookbrand.com/img/assets/asset.f.logo.lg.png" />}
                rightIcon={<CommunicationChatBubble />} />
              <Divider/>
              <ListItem
                primaryText="Project Manager"
                secondaryText="SEE Change Institute"
                leftAvatar={<Avatar src="http://cdn.shopify.com/s/files/1/0259/9491/files/apple_logo_grey_-_small__29926.jpg?868" />}
                rightIcon={<CommunicationChatBubble />} />
              <Divider/>
              <ListItem
                primaryText="Research Associate"
                secondaryText="Aerial Intelligence"
                leftAvatar={<Avatar src="https://cdn4.iconfinder.com/data/icons/new-google-logo-2015/400/new-google-favicon-128.png" />}
                rightIcon={<CommunicationChatBubble />} />
              <Divider/>
              <ListItem
                primaryText="Project Manager"
                secondaryText="SEE Change Institute"
                leftAvatar={<Avatar src="https://www.facebookbrand.com/img/assets/asset.f.logo.lg.png" />}
                rightIcon={<CommunicationChatBubble />} />
              <Divider/>
              <ListItem
                primaryText="Project Manager"
                secondaryText="SEE Change Institute"
                leftAvatar={<Avatar src="http://cdn.shopify.com/s/files/1/0259/9491/files/apple_logo_grey_-_small__29926.jpg?868" />}
                rightIcon={<CommunicationChatBubble />} />
              <Divider/>
            </List>
      </AppCanvas>
    </div>

      );
    }
  });

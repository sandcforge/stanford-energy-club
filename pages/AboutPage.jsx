const {
  AppCanvas,
  Avatar,
  AppBar,
  Styles,
  RaisedButton,
  FlatButton,
  IconButton,
  Menu,
  MenuItem,
  MobileTearSheet,
  ListItem,
  List,
  Divider,
  TextField
} = MUI;

const {
  ImageDehaze,
  NavigationClose,
  ActionAccountCircle,
  ActionInfo,
  CommunicationChatBubble
} = SvgIcons;

const { ThemeManager, LightRawTheme, Colors } = Styles;

injectTapEventPlugin();

const slideShowImages = [
  {
    original: 'http://lorempixel.com/1000/600/nature/1/',
    thumbnail: 'http://lorempixel.com/250/150/nature/1/',
  },
  {
    original: 'http://lorempixel.com/1000/600/nature/2/',
    thumbnail: 'http://lorempixel.com/250/150/nature/2/'
  },
  {
    original: 'http://lorempixel.com/1000/600/nature/3/',
    thumbnail: 'http://lorempixel.com/250/150/nature/3/'
  }
];

AboutPage = React.createClass({

  render() {
    const divFrameStyle = {
      //backgroundColor: Colors.grey900,
      textAlign: 'center',
      flex: 1,
      maxWidth:'100%', //magic to be adptive to the width of the window
      //padding :10,
    };

    const frameStyle = {
      textAlign: 'center',
      justifyContent: 'center',
      flex: 1,
    };


  return (
    <div>
      <Header title="About" />
      <br/>
      <SwiperComponent
        options={{centeredSlides:true,
                  autoplay:0}}
        style={frameStyle}
        swiperIsInitialized={this.swiperIsInitialized}>

        <img style={divFrameStyle} src={slideShowImages[0].original}/>
        <img style={divFrameStyle} src={slideShowImages[1].original}/>
        <img style={divFrameStyle} src={slideShowImages[2].original}/>
        <div style={divFrameStyle} >adfasdf</div>
        <div style={divFrameStyle} >adfasdf</div>
      </SwiperComponent>

      <Footer />
    </div>

  );
    }
  });

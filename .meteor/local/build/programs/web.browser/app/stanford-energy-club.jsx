(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// stanford-energy-club.jsx                                            //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
if (Meteor.isClient) {                                                 // 1
  // This code is executed on the client only                          //
                                                                       //
  Meteor.startup(function () {                                         // 4
    // Use Meteor.startup to render the component after the page is ready
    React.render(React.createElement(App, null), document.getElementById("root-render-target"));
  });                                                                  //
}                                                                      //
                                                                       //
if (Meteor.isServer) {                                                 // 10
  Meteor.startup(function () {                                         // 11
    // code to run on server at startup                                //
  });                                                                  //
}                                                                      //
/////////////////////////////////////////////////////////////////////////

}).call(this);

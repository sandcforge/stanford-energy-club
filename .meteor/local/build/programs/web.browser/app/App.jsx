(function(){

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// App.jsx                                                             //
//                                                                     //
/////////////////////////////////////////////////////////////////////////
                                                                       //
App = React.createClass({                                              // 1
  displayName: "App",                                                  //
                                                                       //
  render: function () {                                                // 2
    return React.createElement(                                        // 3
      "div",                                                           //
      null,                                                            //
      React.createElement(                                             //
        "main",                                                        //
        null,                                                          //
        this.props.content                                             //
      )                                                                //
    );                                                                 //
  }                                                                    //
});                                                                    //
/////////////////////////////////////////////////////////////////////////

}).call(this);

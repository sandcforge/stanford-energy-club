const { IconMenu, MenuItem, IconButton, AppBar,  Styles } = MUI;
const { ImageDehaze, NavigationClose, ActionAccountCircle } = SvgIcons;
const { Colors } = Styles;

injectTapEventPlugin();

Header = React.createClass({
  mixins: [ReactMeteorData],
  getMeteorData() {
     return {
       currentUser: Meteor.user()
     };
  },

  render() {
    const leftIconButton = (
      <IconButton
        onTouchTap={ () => this.refs.refLeftMenu.handleToggle() }
      >
        <ImageDehaze color={Colors.white}/>
      </IconButton>
    )
    console.log(Meteor.userId());

    var rightIconButton;
    if ( this.data.currentUser == null ) {
      //rightIcon = (<Avatar icon={<ActionAccountCircle color={Colors.white}/>} />);
      rightIconButton = (
        <IconButton
          tooltip={'Sign In'}
          onTouchTap={ () => this.refs.refLoginDialog._handleOpen() }
        >
          <ActionAccountCircle color={Colors.white}/>
        </IconButton>
      );
    }
    else {
      rightIconButton = (
        <IconButton
          tooltip={this.data.currentUser.username}

        >
          <IconMenu iconButtonElement={<ActionAccountCircle color={Colors.blueGrey600}/>}>
            <MenuItem primaryText="Profile" />
            <MenuItem
              primaryText="Sign out"
              onTouchTap={ () => Meteor.logout() }
            />
          </IconMenu>
        </IconButton>
      );
      //rightIcon = (<Avatar style={{marginRight:-8, marginTop:-8}}>{this.data.currentUser.username[0]}</Avatar>);
    }


    return (
      <div>
      <AppBar
        title={this.props.title}
        iconElementRight={rightIconButton}
        iconElementLeft={leftIconButton}
        />
      <LeftMenu ref='refLeftMenu'  />
      <Login ref='refLoginDialog' />
      </div>
    );
  }
});

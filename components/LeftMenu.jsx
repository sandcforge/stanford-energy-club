const {
  LeftNav,
  MenuItem
} = MUI;

injectTapEventPlugin();

LeftMenu = React.createClass({

  getInitialState() {
    return {isOpened: false};
  },

  handleToggle() {
    //console.log('LeftMenu handleToggle');
    this.setState({isOpened: !this.state.isOpened});
  },

  handleRequestChange(open) {
    this.setState(open)
  },

  render() {
    return (
      <LeftNav
        docked={false}
        width={200}
        open={this.state.isOpened}
        onRequestChange={open => this.setState({open})}
        >
        <MenuItem onTouchTap={() => FlowRouter.go('/') }>Home</MenuItem>
        <MenuItem >Events</MenuItem>
        <MenuItem onTouchTap={() => FlowRouter.go('/jobs') }>Jobs</MenuItem>
        <MenuItem >Journal</MenuItem>
        <MenuItem onTouchTap={() => FlowRouter.go('/about') }>About</MenuItem>
        <MenuItem onTouchTap={() => this.setState({isOpened: false}) }>Close</MenuItem>
      </LeftNav>
    );
  }
});

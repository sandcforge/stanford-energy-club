const { Paper, Styles } = MUI
const { Colors } = Styles;


const footerFrameStyle = {
  backgroundColor: Colors.grey900,
  textAlign: 'center',
  paddingBottom: 16,
  paddingTop: 16

}

const footerTextStyle = {
    //fontFamily: 'Roboto, sans-serif',
    color:Colors.white
}

Footer = React.createClass({
  render() {
    return (
      <Paper style={footerFrameStyle} zDepth={1}>
        <div style={footerTextStyle}>© 2016 Stanford Energy Club. All rights reserved  </div>
      </Paper>
    );
  }
});

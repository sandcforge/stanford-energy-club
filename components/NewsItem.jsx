const {
  FlatButton,
  Card,
  CardText,
  CardActions,
  CardHeader,
  CardMedia,
  CardTitle,
} = MUI;

injectTapEventPlugin();

NewsItem = React.createClass({
  render() {
    return (
      <Card>
        <CardMedia overlay={<CardTitle title="Vail Global Energy Forum Video Competition" subtitle="8 November 2015 | Blog, News"/>}>
          <img src="https://energyclub.stanford.edu/wp-content/uploads/2015/11/DSC00206-200x120.jpg"/>
        </CardMedia>
        <CardText>
          The Precourt Institute and Stanford Energy Club are excited to announce the Vail Global Energy Forum Video Competition! Winners will receive registration, airfare, and accommodations for the conference on January 29-31 2016 in Beaver Creek, Colorado. This is an excellent opportunity to showcase student research and network with distinguished professionals. The video contest is open […]
        </CardText>
        <CardActions>
          <FlatButton label="Like"/>
          <FlatButton label="Follow"/>
        </CardActions>
      </Card>
    );
  }
});

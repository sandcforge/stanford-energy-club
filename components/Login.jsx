const { AppCanvas, Styles, Dialog,IconButton,AppBar } = MUI
const { ImageDehaze, NavigationClose, ActionAccountCircle } = SvgIcons;
const { Colors } = Styles;

injectTapEventPlugin();

Accounts.ui.config({
   passwordSignupFields: "USERNAME_ONLY"
 });

Login = React.createClass({
  getInitialState() {
    return {isOpened: false};
  },
  _handleOpen() {
    this.setState({isOpened: true});
  },
  _handleClose() {
    this.setState({isOpened: false});
  },

  render() {
    const rightIconButton = (
      <IconButton
        onTouchTap={ this._handleClose }
      >
        <NavigationClose color={Colors.white}/>
      </IconButton>
    )

    return (
        <AppCanvas>
        <Dialog
          modal={true}
          open={this.state.isOpened}
        >
          <AppBar
            title="Login"
            iconElementRight={rightIconButton}
            iconElementLeft={<div></div>}
          />
          <Accounts.ui.LoginFormSet redirect={ this._handleClose }/>
        </Dialog>
      </AppCanvas>

    );
  }
});
